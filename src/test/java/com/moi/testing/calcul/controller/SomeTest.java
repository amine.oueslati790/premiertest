package com.moi.testing.calcul.controller;

import static org.assertj.core.api.Assertions.assertThat;

import javax.inject.Inject;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SomeTest {

	@Inject
	private HomeController controller;

	@Test
	void test() {
		assertThat(controller).isNotNull();
	}

}

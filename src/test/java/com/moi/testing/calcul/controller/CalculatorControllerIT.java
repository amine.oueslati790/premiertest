package com.moi.testing.calcul.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import javax.inject.Inject;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.moi.testing.calcul.domain.Calculator;
import com.moi.testing.calcul.service.CalculationService;
import com.moi.testing.calcul.service.SolutionFormatter;

@WebMvcTest(controllers = { CalculatorController.class, CalculationService.class })
@ExtendWith(SpringExtension.class)
class CalculatorControllerIT {

	@Inject
	private MockMvc mockMvc;

	@MockBean
	private Calculator calculator;

	@MockBean
	private SolutionFormatter formatter;

	@Test
	void givenCalculatorApp_ShouldRetrunSolution_whenRequestAdd() throws Exception {
		// Given
		when(calculator.add(2, 3)).thenReturn(5);

		// When

		final MvcResult result = mockMvc
				.perform(MockMvcRequestBuilders.post("/calculator").param("leftArgument", "2")
						.param("rightArgument", "3").param("calculationType", "ADDITION"))
				.andDo(print())
				.andExpect(MockMvcResultMatchers.status().is2xxSuccessful()).andReturn();
		// Then
		assertThat(result.getResponse().getContentAsString()).contains("id=\"solution\"").contains(">5</span");
		verify(calculator).add(2, 3);
		verify(formatter).format(5);
	}

}

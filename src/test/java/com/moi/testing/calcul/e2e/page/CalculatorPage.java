package com.moi.testing.calcul.e2e.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CalculatorPage {

	@FindBy(id = "submit")
	WebElement submitButton;

	@FindBy(id = "left")
	WebElement leftArgument;

	@FindBy(id = "right")
	WebElement rightArgument;

	@FindBy(id = "type")
	WebElement calculationType;

	@FindBy(id = "solution")
	WebElement solution;

	private final WebDriver webDriver;

	public static final String ADDITION_SYMBOL = "+";
	public static final String SUBTRACTION_SYMBOL = "-";
	public static final String DIVISION_SYMBOL = "/";
	private static final String MULTIPLICATION_SYMBOLE = "x";

	public CalculatorPage(WebDriver webDriver) {
		this.webDriver = webDriver;
		PageFactory.initElements(webDriver, this);
	}

	public String multiply(String leftValue, String rightValue) {
		return calculate(MULTIPLICATION_SYMBOLE, leftValue, rightValue);
	}

	private String calculate(String calculationTypeValue, String leftValue, String rightValue) {
		leftArgument.sendKeys(leftValue);
		rightArgument.sendKeys(rightValue);
		calculationType.sendKeys(calculationTypeValue);
		submitButton.click();

		final WebDriverWait waiter = new WebDriverWait(webDriver, 5);
		waiter.until(ExpectedConditions.visibilityOf(solution));

		return solution.getText();
	}

	public String add(String leftValue, String rightValue) {
		return calculate(ADDITION_SYMBOL, leftValue, rightValue);
	}

}

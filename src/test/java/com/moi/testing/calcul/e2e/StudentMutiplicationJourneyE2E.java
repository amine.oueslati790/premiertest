package com.moi.testing.calcul.e2e;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.moi.testing.calcul.e2e.page.CalculatorPage;

import io.github.bonigarcia.wdm.WebDriverManager;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class StudentMutiplicationJourneyE2E {

	@LocalServerPort
	private Integer port;
	private WebDriver driver;
	private String baseUrl;

	@BeforeAll
	public static void setUpFirefoxDriver() {
		WebDriverManager.firefoxdriver().setup();
	}

	@BeforeEach
	public void setUpWebDriver() {
		driver = new FirefoxDriver();
		baseUrl = "http://localhost:" + port + "/calculator";
	}

	@AfterEach
	public void quitWebDriver() {
		if (driver != null) {
			driver.quit();
		}
	}

	@Test
	void aStudentUsesTheCalculatorToMultiplyTwoBySixteen() {

		// Given
		driver.get(baseUrl);
		final CalculatorPage calculatorPage = new CalculatorPage(driver);

		// When
		final String solution = calculatorPage.multiply("2", "16");

		// Then
		assertThat(solution).isEqualTo("32");
	}
}

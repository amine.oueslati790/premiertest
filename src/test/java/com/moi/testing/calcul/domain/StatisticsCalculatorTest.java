package com.moi.testing.calcul.domain;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import com.moi.testing.calcul.domain.StatisticsCalculator;

@ExtendWith(MockitoExtension.class)
class StatisticsCalculatorTest {

	@Spy
	IntSummaryStatistics intSummaryStatistics = new IntSummaryStatistics();

	StatisticsCalculator statisticsCalculatorUndertest;

	@BeforeEach
	void setUp() {
		statisticsCalculatorUndertest = new StatisticsCalculator(intSummaryStatistics);
	}

	@Test
	void average_shouldReturnTheMean_ofAListOfIntegers() {
		// Given
		final List<Integer> samples = Arrays.asList(8, 16, 9, 5);

		// When
		final int result = statisticsCalculatorUndertest.average(samples);

		// Then
		assertThat(result).isEqualTo(9);
	}

	@Test
	void average_shouldSample_allIntegersProvided() {
		// Given
		final List<Integer> samples = Arrays.asList(8, 16, 9, 5);
		final ArgumentCaptor<Integer> argumentCaptor = ArgumentCaptor.forClass(Integer.class);

		// When
		statisticsCalculatorUndertest.average(samples);

		verify(intSummaryStatistics, times(samples.size())).accept(argumentCaptor.capture());
		final List<Integer> integers = argumentCaptor.getAllValues();

		assertThat(integers).containsExactly(samples.toArray(new Integer[0]));
	}

	@Test
	void WhenAverage_shouldSample_allIntegersProvided() {
		final List<Integer> samples = Arrays.asList(1, 5, 9, 7, 6, 12);
		final ArgumentCaptor<Integer> argumentCaptor = ArgumentCaptor.forClass(Integer.class);

		statisticsCalculatorUndertest.average(samples);
		verify(intSummaryStatistics, times(samples.size())).accept(argumentCaptor.capture());
		final List<Integer> integers = argumentCaptor.getAllValues();

		assertThat(integers).containsExactly(samples.toArray(new Integer[0]));
	}

}

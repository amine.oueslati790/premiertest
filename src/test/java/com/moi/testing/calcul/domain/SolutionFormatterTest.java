package com.moi.testing.calcul.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.moi.testing.calcul.service.SolutionFormatter;
import com.moi.testing.calcul.service.SolutionFormatterImpl;

class SolutionFormatterTest {
	private SolutionFormatter solutionFormatter;

	@BeforeEach
	public void initFormatter() {
		solutionFormatter = new SolutionFormatterImpl();
	}

	@Test
	void format_shouldFormatAnyBigNumber() {
		// GIVEN
		final int number = 1234567890;

		// WHEN
		final String result = solutionFormatter.format(number);

		// THEN
		assertThat(result).isEqualTo("1 234 567 890");
	}

}

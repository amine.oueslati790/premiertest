package com.moi.testing.calcul.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

class DoubleCalculatorTest {

	private Calculator calculatorUnderTest;

	@BeforeEach
	public void init() {
		calculatorUnderTest = new Calculator();
	}

	@Test
	@Disabled("ticket")
	void Sub_shouldReturnTheResult_ofTwoDoubleNumbers() {
		// Given

		// When
		final double result = calculatorUnderTest.sub(1.0000000001, 1.0);

		// Then
		if (result != 0.0000000001) {
			throw new AssertionError(String.format("Erreur :\nAttendu :  0,0000000001\n R�sultat : %.10f", result));
		}
	}

}

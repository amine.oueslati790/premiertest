package com.moi.testing.calcul.domain;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Set;

import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

@ExtendWith(LoggingExtension.class)
class CalculatorTest {

	private static Instant startedAt;

	private Calculator calculator;

	private Logger logger;

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	@BeforeAll
	public static void initTime() {
		startedAt = Instant.now();
		System.out.println("started at: " + startedAt);
	}

	@AfterAll
	public static void calculTime() {
		final Instant finishedAt = Instant.now();
		final long duration = Duration.between(startedAt, finishedAt).toMillis();
		System.out.println("Duree de traitement des tests: " + duration);
	}

	@BeforeEach
	public void intiCalculator() {
		logger.info("Appel avant chaque test");
		calculator = new Calculator();
	}

	@AfterEach
	public void undefCalculator() {
		logger.info("Appel apr�s chaque test");
		calculator = null;
	}

	@Test
	@Tag("QuatreOperation")
	void testAddTwoPositiveNumbers() {
		// Arrange
		final int a = 1;
		final int b = 2;

		// Act
		final int result = calculator.add(a, b);

		// Assert
		assertEquals(3, result);
	}

	@Test
	@Tag("QuatreOperation")
	void add_shouldReturnSum_OfPositiveAndNegativeNumber() {
		// Arrage
		final int a = 10;
		final int b = -6;

		// Act
		final int result = calculator.add(a, b);

		// Assert
		assertThat(result).isEqualTo(4);
	}

	@Test
	@Tag("QuatreOperation")
	void add_shouldRetunSum_ofTwoDouble() {
		// Arrange
		final double a = 3.0;
		final double b = 3.0;

		// Act
		final double result = calculator.add(a, b);

		// Assert
		assertThat(result).isEqualTo(6.0);
	}

	@Test
	void sub_shouldReturnSubstraction_OfTwoPositiveNumbers() {
		final int a = 10;
		final int b = 5;

		final int result = calculator.sub(a, b);

		assertThat(result).isEqualTo(5);
	}

	@Test
	void sub_shouldReturnSubstraction_ofTwoDouble() {
		final double a = 10.3;
		final double b = -5.5;

		final double result = calculator.sub(a, b);

		assertThat(result).isEqualTo(15.8);
	}

	@Test
	void sub_shouldReturnSubstraction_OfPositiveAndNegativeNumbers() {
		final int a = 10;
		final int b = -5;

		final int result = calculator.sub(a, b);

		assertThat(result).isEqualTo(15);
	}

	@Test
	void sub_shouldReturnSubstraction_OfTwoNegativeNumbers() {
		final int a = -10;
		final int b = -5;

		final int result = calculator.sub(a, b);

		assertThat(result).isEqualTo(-5);
	}

	@Test
	@Tag("QuatreOperation")
	void multiplyTwoPositiveNumber() {
		// Arrange
		final int a = 3;
		final int b = 6;

		// Act
		final int result = calculator.multiply(a, b);

		// Assert
		assertEquals(18, result);
	}

	@Test
	@Tag("QuatreOperation")
	void multiplyTwoPositiveDouble() {
		// Arrange
		final double a = 3.5;
		final double b = 6.5;

		// Act
		final double result = calculator.multiply(a, b);

		// Assert
		assertEquals(22.75, result);
	}

	@Tag("QuatreOperation")
	@ParameterizedTest(name = "{0} * 0 should be equal to 0")
	@ValueSource(ints = { 1, 3, 45, 789 })
	void multiply_shouldReturnZero_ofZeroMultiplyPositiveNumber(int arg) {
		// Arrange

		// Act
		final int result = calculator.multiply(0, arg);

		// Assert
		assertEquals(0, result);
	}

	@Tag("QuatreOperation")
	@ParameterizedTest(name = "{0} + {1} should be equal to {2}")
	@CsvSource({ "1,2,3", "6,9,15" })
	void add_shouldReturnSum_ofTwoPositiveNumber(int arg1, int arg2, int arg3) {
		// Arrange

		// Act
		final int result = calculator.add(arg1, arg2);

		// Assert
		assertEquals(arg3, result);
	}

	@Timeout(1)
	@Test
	void longcalcul_shouldComputeInLessThanOneSecond() {
		calculator.longCalcul();

	}

	@Test
	void DigitSets_shouldReturnSet_ofDegitsOfPoitiveNumbers() {
		// Arrange
		final int number = 1153947;

		// Act
		final Set<Integer> actualDigits = calculator.digitSets(number);

		// Assert
		assertThat(actualDigits).containsExactlyInAnyOrder(1, 5, 3, 9, 4, 7);
	}

	@Test
	void DigitSets_shouldReturnSet_ofDegitsOfNegativeNumberst() {
		// Arrange
		final int number = -1234568;

		// Act
		final Set<Integer> actualDegits = calculator.digitSets(number);

		// Assert
		assertThat(actualDegits).containsExactlyInAnyOrder(1, 2, 3, 4, 5, 6, 8);
	}

	@Test
	void DegitSets_shouldReturnExactlyZero() {
		// Arrange
		final int number = 0;

		// Act
		final Set<Integer> actualDegits = calculator.digitSets(number);

		// Assert
		assertThat(actualDegits).containsExactly(0);
	}

	@Disabled("�chou chaque mardi")
	@Test
	void testDate() {
		// Arrange
		final LocalDateTime date = LocalDateTime.now();

		// Assert
		assertThat(date.getDayOfWeek()).isNotEqualTo(DayOfWeek.TUESDAY);
	}

	@Test
	void fact12_shouldRetrunstheCorrectAnswer() {
		// arrange
		final int a = 12;

		// act
		final int result = calculator.fact(a);

		// assert
		assertThat(result).isEqualTo(12 * 11 * 10 * 9 * 8 * 7 * 6 * 5 * 4 * 3 * 2);
	}

}

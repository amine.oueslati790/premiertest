package com.moi.testing.calcul.domain;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.stream.Stream;

import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import com.moi.testing.calcul.domain.model.CalculationModel;

@ExtendWith(LoggingExtension.class)
class BatchCalculatorTest {

	private Logger logger;

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	@Test
	void batchCalculation_ReturnCorrectAnswerList_ofOperationList() throws FileNotFoundException, IOException {

		Stream<String> operations;

		try (BufferedReader br = new BufferedReader(
				new FileReader("F:\\workspace\\premiertest\\src\\test\\resources\\dataSet/operation.txt"))) {
			operations = br.lines();

			final BatchCalculator batchCalculator = new BatchCalculator(new Calculator());

			final List<CalculationModel> model = batchCalculator.calculate(operations);

			assertThat(model).extracting("solution").containsExactlyInAnyOrder(4, 30);
		}

	}

}

package com.moi.testing.calcul.domain;

import static java.lang.Math.PI;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.withinPercentage;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import com.moi.testing.calcul.domain.ConversionCalculator;

@Tag("ConversionTest")
@DisplayName("R�ussir � convertir entre diff�rentes unit�s.")
class ConversionCalculatorTest {

	private final ConversionCalculator conversionCalculator = new ConversionCalculator();

	@Nested
	@Tag("TemperatureTests")
	@DisplayName("R�ussir � convertir des temp�ratures")
	class TemperatureTests {

		@Test
		@DisplayName("Soit une T� � 0�C, lorsque l'on convertit en �F, alors on obtient 32�F")
		void celsiusToFahrenheit_returnAFahreinheitTemperature_whenCelsiusIsZero() {
			// Act
			final Double actualFahreinheit = conversionCalculator.celsiusToFahrenheit(0.0);

			// Assert
			assertThat(actualFahreinheit).isCloseTo(32.0, withinPercentage(0.01));
		}

		@Test
		@DisplayName("Soit une T� � 32�F, lorsque l'on convertit en �C, alors on obtient 0�C")
		void fahrenheitToCelsius_returnACelsiusTemperature_whenFahrenheitIsZero() {
			// Act
			final Double actualFahreinheit = conversionCalculator.fahrenheitToCelsius(32.0);

			// Assert
			assertThat(actualFahreinheit).isCloseTo(0.0, withinPercentage(0.01));
		}
	}

	@Test
	@DisplayName("Soit un volume de 3.78541 litres, en gallons, on obtient 1 gallon.")
	void litresToGallon_returnsOneGallon_whenConvertingTheEquivalentLitres() {
		// Act
		final Double actualLitres = conversionCalculator.litresToGallon(3.78541);

		// Assert
		assertThat(actualLitres).isCloseTo(1.0, withinPercentage(0.01));
	}

	@Test
	@DisplayName("L'aire d'un disque de rayon 1 doit valoir PI.")
	void radiusToAreaOfCircle_returnsPi_whenWeHaveARadiusOfOne() {
		final Double actualArea = conversionCalculator.radiusToAreaOfCircle(1.0);
		assertThat(actualArea).isCloseTo(PI, withinPercentage(0.01));
	}
}

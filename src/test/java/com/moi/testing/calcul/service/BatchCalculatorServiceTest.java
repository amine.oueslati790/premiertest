package com.moi.testing.calcul.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import com.moi.testing.calcul.domain.Calculator;
import com.moi.testing.calcul.domain.model.CalculationModel;
import com.moi.testing.calcul.domain.model.CalculationType;
import com.moi.testing.calcul.service.BatchCalculatorService;
import com.moi.testing.calcul.service.BatchCalculatorServiceImpl;
import com.moi.testing.calcul.service.CalculationService;
import com.moi.testing.calcul.service.CalculationServiceImpl;

@ExtendWith(MockitoExtension.class)
class BatchCalculatorServiceTest {

	@Mock
	private CalculationService calculationService;

	private BatchCalculatorService batchCalculatorServiceNoMock;

	private BatchCalculatorService batchCalculatorServiceUnderTest;

	@BeforeEach
	void init() {
		batchCalculatorServiceUnderTest = new BatchCalculatorServiceImpl(calculationService);
		batchCalculatorServiceNoMock = new BatchCalculatorServiceImpl(
				new CalculationServiceImpl(new Calculator(), new SolutionFormatterImpl()));
	}

	@Test
	void givenOperationsList_whenbatchCalculate_thenReturnsCorrectAnswerList()
			throws IOException, URISyntaxException {
		// GIVEN
		final Stream<String> operations = Arrays.asList("2 + 2", "5 - 4", "6 x 8", "9 / 3").stream();

		// WHEN
		final List<CalculationModel> results = batchCalculatorServiceNoMock.batchCalculate(operations);

		// THEN
		assertThat(results).extracting(CalculationModel::getSolution).containsExactly(4, 1, 48, 3);
	}

	@Test
	void givenOperationsList_whenbatchCalculate_thenCallsServiceWithCorrectArguments() {
		// GIVEN
		final Stream<String> operations = Arrays.asList("2 + 2", "5 - 4", "6 x 8", "9 / 3").stream();
		final ArgumentCaptor<CalculationModel> calculationModelCaptor = ArgumentCaptor.forClass(CalculationModel.class);

		// WHEN
		batchCalculatorServiceUnderTest.batchCalculate(operations);

		// THEN
		verify(calculationService, times(4)).calculate(calculationModelCaptor.capture());
		final List<CalculationModel> calculationModels = calculationModelCaptor.getAllValues();

		assertThat(calculationModels).extracting(CalculationModel::getLeftArgument, CalculationModel::getType,
				CalculationModel::getRightArgument)
				.containsExactly(tuple(2, CalculationType.ADDITION, 2),
						tuple(5, CalculationType.SUBSTRUCTION, 4),
						tuple(6, CalculationType.MULTIPLICATION, 8),
						tuple(9, CalculationType.DIVISION, 3));

	}

	@Test
	void givenOperationsList_whenbatchCalculate_thenCallsServiceAndReturnsAnswer() {
		final Stream<String> operations = Arrays.asList("2 + 2", "5 - 4", "6 x 8", "9 / 3").stream();

		when(calculationService.calculate(any(CalculationModel.class))).then(invocation -> {
			final CalculationModel model = invocation.getArgument(0);
			switch (model.getType()) {
			case ADDITION:
				model.setSolution(4);
				break;
			case SUBSTRUCTION:
				model.setSolution(1);
				break;
			case MULTIPLICATION:
				model.setSolution(48);
				break;
			case DIVISION:
				model.setSolution(3);
				break;

			default:
				break;
			}
			return model;
		});

		final List<CalculationModel> results = batchCalculatorServiceUnderTest.batchCalculate(operations);

		verify(calculationService, times(4)).calculate(any(CalculationModel.class));
		assertThat(results).extracting("solution").contains(4, 1, 48, 3);
	}

	@Test
	void operationsList_whenbatchCalculate_thenCallsServiceWithCorrectArguments() {
		// Given
		final Stream<String> operations = Arrays.asList("1 + 2", "3 / 3").stream();
		final ArgumentCaptor<CalculationModel> argumentCaptor = ArgumentCaptor.forClass(CalculationModel.class);

		// When
		batchCalculatorServiceUnderTest.batchCalculate(operations);

		// Then
		verify(calculationService, times(2)).calculate(argumentCaptor.capture());
		final List<CalculationModel> calculationModels = argumentCaptor.getAllValues();
		assertThat(calculationModels)
				.extracting(CalculationModel::getLeftArgument, CalculationModel::getType,
						CalculationModel::getRightArgument)
				.containsExactly(tuple(1, CalculationType.ADDITION, 2), tuple(3, CalculationType.DIVISION, 3));

	}

	@Test
	void opperationsList_whenbatchCalculate_thenCallsServiceAndReturnsAnswer() {
		final Stream<String> operation = Arrays.asList("1 + 2", "3 / 3").stream();

		// When
		when(calculationService.calculate(any(CalculationModel.class))).then(answer());

		final List<CalculationModel> calculationModels = batchCalculatorServiceUnderTest.batchCalculate(operation);

		// Then
		verify(calculationService, times(2)).calculate(any(CalculationModel.class));
		assertThat(calculationModels).extracting(CalculationModel::getSolution).containsExactly(3, 1);

	}

	private Answer<?> answer() {
		return invocation -> {
			final CalculationModel calculationModel = invocation.getArgument(0, CalculationModel.class);
			switch (calculationModel.getType()) {
			case ADDITION:
				calculationModel.setSolution(3);
				break;
			case DIVISION:
				calculationModel.setSolution(1);
				break;

			default:
				break;
			}
			return calculationModel;
		};
	}

	@Test
	void operationsList_whenbatchCalculate_thenCallsServiceAndReturnsAnswer2() {
		final Stream<String> operations = Arrays.asList("1 + 2", "3 / 3").stream();

		when(calculationService.calculate(any(CalculationModel.class)))
				.thenReturn(new CalculationModel(1, 2, CalculationType.ADDITION, 3))
				.thenReturn(new CalculationModel(3, 3, CalculationType.DIVISION, 1));

		final List<CalculationModel> calculationModels = batchCalculatorServiceUnderTest.batchCalculate(operations);

		verify(calculationService, times(2)).calculate(any(CalculationModel.class));
		assertThat(calculationModels).extracting("solution").containsExactly(3, 1);
	}
}

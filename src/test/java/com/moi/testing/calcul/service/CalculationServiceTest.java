package com.moi.testing.calcul.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.moi.testing.calcul.domain.Calculator;
import com.moi.testing.calcul.domain.model.CalculationModel;
import com.moi.testing.calcul.domain.model.CalculationType;

@ExtendWith(MockitoExtension.class)
class CalculationServiceTest {

	@Mock
	private Calculator calculator;

	@Mock
	private SolutionFormatter formatter;

	CalculationService calculationServiceUnderTest;

	@BeforeEach
	void init() {
		calculationServiceUnderTest = new CalculationServiceImpl(calculator, formatter);
	}

	@Test
	void add_returnTheSum_OfTwoPositiveNumbers() {
		// Given
		when(calculator.add(1, 2)).thenReturn(3);

		// When
		final int result = calculationServiceUnderTest.calculate(new CalculationModel(1, 2, CalculationType.ADDITION))
				.getSolution();

		// Then
		verify(calculator).add(1, 2);
		assertThat(result).isEqualTo(3);
	}

	@Test
	void add_returnTheSum_OfTwoNegativeNumbers() {
		// Given
		when(calculator.add(-1, -2)).thenReturn(-3);

		// When
		final int result = calculationServiceUnderTest.calculate(new CalculationModel(-1, -2, CalculationType.ADDITION))
				.getSolution();

		// Then
		verify(calculator, times(1)).add(-1, -2);
		assertThat(result).isEqualTo(-3);
	}

	@Test
	void add_returnTheSum_OfTwoZero() {
		// Given
		when(calculator.add(0, 0)).thenReturn(0);

		// When
		final int result = calculationServiceUnderTest.calculate(new CalculationModel(0, 0, CalculationType.ADDITION))
				.getSolution();

		// Then
		verify(calculator).add(0, 0);
		assertThat(result).isZero();
	}

	@Test
	void calculate_shouldUseCalculator_forAnyAddition() {

		// Given
		when(calculator.add(any(Integer.class), any(Integer.class))).thenReturn(3);

		// When
		final int result = calculationServiceUnderTest.calculate(new CalculationModel(1, 2, CalculationType.ADDITION))
				.getSolution();

		// Then
		verify(calculator).add(any(Integer.class), any(Integer.class));
		assertThat(result).isEqualTo(3);
	}

	@Test
	void calculate_shouldThrowIllegalArgumentException_forADivisionBy0() {
		// WHEN
		when(calculator.divide(1, 0)).thenThrow(new ArithmeticException());

		// THEN
		assertThrows(IllegalArgumentException.class, calculate());

		verify(calculator, times(1)).divide(1, 0);
	}

	private Executable calculate() {
		return () -> calculationServiceUnderTest.calculate(
				new CalculationModel(1, 0, CalculationType.DIVISION));
	}

	@Test
	void calculate_shouldFormatSolution_forAnAddition() {

		// GIVEN
		when(calculator.add(10000, 3000)).thenReturn(13000);
		when(formatter.format(13000)).thenReturn("13 000");

		// WHEN
		final String formattedResult = calculationServiceUnderTest
				.calculate(new CalculationModel(10000, 3000, CalculationType.ADDITION))
				.getFormattedSolution();

		// THEN
		assertThat(formattedResult).isEqualTo("13 000");

	}
}

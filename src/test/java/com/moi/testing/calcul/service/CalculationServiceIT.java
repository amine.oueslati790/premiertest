package com.moi.testing.calcul.service;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.moi.testing.calcul.domain.Calculator;
import com.moi.testing.calcul.domain.model.CalculationModel;
import com.moi.testing.calcul.domain.model.CalculationType;

class CalculationServiceIT {

	CalculationServiceImpl calculationServiceImpl;

	@BeforeEach
	void setup() {
		calculationServiceImpl = new CalculationServiceImpl(new Calculator(), new SolutionFormatterImpl());
	}

	@Test
	void calculationSerice_shouldCalculateASolution_whenGivenACalculationModel() {
		// Given
		final CalculationModel calculationModel = new CalculationModel(1, 2, CalculationType.ADDITION);

		// When
		final CalculationModel result = calculationServiceImpl.calculate(calculationModel);

		// then
		assertThat(result).extracting(CalculationModel::getSolution).isEqualTo(3);
	}

}

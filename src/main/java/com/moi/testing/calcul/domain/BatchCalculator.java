package com.moi.testing.calcul.domain;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.moi.testing.calcul.domain.model.CalculationModel;
import com.moi.testing.calcul.domain.model.CalculationType;

public class BatchCalculator {

	private final Calculator calculator;

	public BatchCalculator(Calculator calculator) {
		this.calculator = calculator;
	}

	public List<CalculationModel> calculate(Stream<String> operations) {

		return operations.map(s -> solve(CalculationModel.fromText(s))).collect(Collectors.toList());
	}

	private CalculationModel solve(CalculationModel fromText) {
		final CalculationType calculationType = fromText.getType();

		Integer solution = null;

		switch (calculationType) {
		case ADDITION:
			solution = calculator.add(fromText.getLeftArgument(), fromText.getRightArgument());
			break;
		case MULTIPLICATION:
			solution = calculator.multiply(fromText.getLeftArgument(), fromText.getRightArgument());
			break;

		default:
			throw new UnsupportedOperationException("Unsupported calculations");
		}
		return new CalculationModel(fromText.getLeftArgument(), fromText.getRightArgument(), calculationType, solution);
	}

}

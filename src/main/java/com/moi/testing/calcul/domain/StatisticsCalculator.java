package com.moi.testing.calcul.domain;

import java.util.IntSummaryStatistics;
import java.util.List;

public class StatisticsCalculator {

	private final IntSummaryStatistics intSummaryStatistics;

	public StatisticsCalculator(IntSummaryStatistics intSummaryStatistics) {
		this.intSummaryStatistics = intSummaryStatistics;
	}

	public int average(List<Integer> samples) {
		samples.forEach(intSummaryStatistics::accept);
		final Double average = intSummaryStatistics.getAverage();

		return average.intValue();
	}

}

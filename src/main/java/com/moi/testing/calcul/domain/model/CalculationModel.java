package com.moi.testing.calcul.domain.model;

public class CalculationModel {

	private static final String SEPARATEUR = " ";
	private final Integer leftArgument;
	private final Integer rightArgument;
	private final CalculationType type;
	private Integer solution;
	private String formattedSolution;

	public CalculationModel(Integer leftArgument, Integer rightArgument, CalculationType type) {
		this.leftArgument = leftArgument;
		this.rightArgument = rightArgument;
		this.type = type;
	}

	public CalculationModel(Integer leftArgument, Integer rightArgument, CalculationType type, Integer solution) {
		this.leftArgument = leftArgument;
		this.rightArgument = rightArgument;
		this.type = type;
		this.solution = solution;
	}

	public static CalculationModel fromText(String s) {
		final String[] parts = s.split(SEPARATEUR);
		final int leftArgument = Integer.parseInt(parts[0]);
		final int rightArgument = Integer.parseInt(parts[2]);
		final CalculationType type = CalculationType.fromSymbol(parts[1]);

		return new CalculationModel(leftArgument, rightArgument, type);
	}

	public Integer getSolution() {
		return solution;
	}

	public void setSolution(Integer solution) {
		this.solution = solution;
	}

	public static String getSeparateur() {
		return SEPARATEUR;
	}

	public Integer getLeftArgument() {
		return leftArgument;
	}

	public Integer getRightArgument() {
		return rightArgument;
	}

	public CalculationType getType() {
		return type;
	}

	public String getFormattedSolution() {
		return formattedSolution;
	}

	public void setFormattedSolution(String formattedSolution) {
		this.formattedSolution = formattedSolution;
	}

}

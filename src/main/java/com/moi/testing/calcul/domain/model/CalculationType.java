package com.moi.testing.calcul.domain.model;

public enum CalculationType {
	ADDITION,
	SUBSTRUCTION,
	MULTIPLICATION,
	DIVISION;

	public static CalculationType fromSymbol(String operation) {
		switch (operation) {
		case "+":
			return ADDITION;
		case "-":
			return SUBSTRUCTION;
		case "*":
		case "x":
			return MULTIPLICATION;
		case "/":
			return DIVISION;
		default:
			throw new UnsupportedOperationException("Not implemented yet");
		}
	}

}

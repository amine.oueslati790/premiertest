package com.moi.testing.calcul.domain;

import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class Calculator {

	Logger logger = LogManager.getLogger(Calculator.class);

	public int add(int a, int b) {
		return a + b;
	}

	public int sub(int a, int b) {
		return a - b;
	}

	public int multiply(int a, int b) {
		return a * b;
	}

	public double add(double a, double b) {
		return a + b;
	}

	public double sub(double d, double e) {
		return d - e;
	}

	public double multiply(double a, double b) {
		return a * b;
	}

	public int divide(int a, int b) {
		return a / b;
	}

	public void longCalcul() {

		try {
			Thread.sleep(500);
		} catch (final InterruptedException e) {
			logger.error(e);
			Thread.currentThread().interrupt();
		}

	}

	public Set<Integer> digitSets(int number) {
		final Set<Integer> integers = new HashSet<>();
		final String stringNumber = String.valueOf(number);

		for (int i = 0; i < stringNumber.length(); i++) {
			if (stringNumber.charAt(i) != '-') {
				integers.add(Integer.parseInt(stringNumber, i, i + 1, 10));
			}
		}

		return integers;
	}

	public int fact(int a) {
		int result;
		if (a == 0) {
			result = 1;
		} else {
			result = a * fact(a - 1);
		}
		return result;
	}

}

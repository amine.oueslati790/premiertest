package com.moi.testing.calcul.domain;

public class ConversionCalculator {

	private static final double FAHRENHEIT_CONST = 32.0;
	private static final double CELSIUS_FAHRENHEIT_CONVERSION_FACTOR = 9.0 / 5.0;
	private static final double LITRE_TO_GALLON_MULTIPLIER = 0.264172;

	private static final double POWER_OF_RADIUS = 2.0;

	public Double celsiusToFahrenheit(double d) {
		return d * CELSIUS_FAHRENHEIT_CONVERSION_FACTOR + FAHRENHEIT_CONST;
	}

	public Double fahrenheitToCelsius(double d) {
		return (d - FAHRENHEIT_CONST) / CELSIUS_FAHRENHEIT_CONVERSION_FACTOR;
	}

	public Double litresToGallon(double d) {
		return Math.ceil(d * LITRE_TO_GALLON_MULTIPLIER);
	}

	public Double radiusToAreaOfCircle(double d) {
		return Math.pow(d, POWER_OF_RADIUS) * Math.PI;
	}

}

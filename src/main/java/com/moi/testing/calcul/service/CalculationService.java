package com.moi.testing.calcul.service;

import com.moi.testing.calcul.domain.model.CalculationModel;

public interface CalculationService {

	CalculationModel calculate(CalculationModel calculationModel);
}

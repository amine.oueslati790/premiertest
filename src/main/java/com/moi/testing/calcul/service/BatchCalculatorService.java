package com.moi.testing.calcul.service;

import java.util.List;
import java.util.stream.Stream;

import com.moi.testing.calcul.domain.model.CalculationModel;

public interface BatchCalculatorService {

	List<CalculationModel> batchCalculate(Stream<String> operations);

}

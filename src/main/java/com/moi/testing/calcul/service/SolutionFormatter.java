package com.moi.testing.calcul.service;

public interface SolutionFormatter {
	String format(int solution);
}

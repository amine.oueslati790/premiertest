package com.moi.testing.calcul.service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.moi.testing.calcul.domain.model.CalculationModel;

public class BatchCalculatorServiceImpl implements BatchCalculatorService {

	private final CalculationService calculationService;

	public BatchCalculatorServiceImpl(CalculationService calculationService) {
		this.calculationService = calculationService;
	}

	@Override
	public List<CalculationModel> batchCalculate(Stream<String> operations) {
		return operations.map(s -> calculationService.calculate(CalculationModel.fromText(s)))
				.collect(Collectors.toList());
	}

}

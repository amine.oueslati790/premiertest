package com.moi.testing.calcul.service;

import java.io.IOException;
import java.util.stream.Stream;

public interface BatchCalculationFileService {

	Stream<String> read(String file) throws IOException;
}

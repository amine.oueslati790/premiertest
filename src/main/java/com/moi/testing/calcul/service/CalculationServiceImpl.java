package com.moi.testing.calcul.service;

import javax.inject.Named;

import com.moi.testing.calcul.domain.Calculator;
import com.moi.testing.calcul.domain.model.CalculationModel;
import com.moi.testing.calcul.domain.model.CalculationType;

@Named
public class CalculationServiceImpl implements CalculationService {

	private final Calculator calculator;

	private final SolutionFormatter formatter;

	public CalculationServiceImpl(Calculator calculator, SolutionFormatter formatter) {
		this.calculator = calculator;
		this.formatter = formatter;
	}

	@Override
	public CalculationModel calculate(CalculationModel calculationModel) {

		final CalculationType type = calculationModel.getType();

		Integer response = null;
		switch (type) {
		case ADDITION:
			response = calculator.add(calculationModel.getLeftArgument(), calculationModel.getRightArgument());
			break;
		case SUBSTRUCTION:
			response = calculator.sub(calculationModel.getLeftArgument(), calculationModel.getRightArgument());
			break;
		case MULTIPLICATION:
			response = calculator.multiply(calculationModel.getLeftArgument(), calculationModel.getRightArgument());
			break;
		case DIVISION:
			try {
				response = calculator.divide(calculationModel.getLeftArgument(), calculationModel.getRightArgument());
			} catch (final Exception e) {
				throw new IllegalArgumentException(e);
			}
			break;
		default:
			throw new UnsupportedOperationException("Unsupported calculations");
		}

		calculationModel.setSolution(response);
		calculationModel.setFormattedSolution(formatter.format(response));
		return calculationModel;
	}

}

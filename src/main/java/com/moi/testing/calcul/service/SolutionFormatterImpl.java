package com.moi.testing.calcul.service;

import java.util.Locale;

import javax.inject.Named;

@Named
public class SolutionFormatterImpl implements SolutionFormatter {

	@Override
	public String format(int solution) {
		/*
		 * final NumberFormat nf = NumberFormat.getInstance(new Locale("sk", "SK"));
		 * nf.setGroupingUsed(true); return nf.format(solution);
		 */

		return String.format(Locale.FRANCE, "%,d", solution);
	}

}

package com.moi.testing.calcul.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class BatchCalculationFileServiceImpl implements BatchCalculationFileService {

	@Override
	public Stream<String> read(String file) throws IOException {

		return Files.lines(Paths.get(file));
	}

}

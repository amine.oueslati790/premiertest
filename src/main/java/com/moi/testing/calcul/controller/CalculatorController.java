package com.moi.testing.calcul.controller;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.moi.testing.calcul.domain.model.Calculation;
import com.moi.testing.calcul.domain.model.CalculationModel;
import com.moi.testing.calcul.domain.model.CalculationType;
import com.moi.testing.calcul.service.CalculationService;

@Controller
public class CalculatorController {

	private static final String CALCULATOR_TEMPLATE = "calculator";

	@Inject
	CalculationService calculationService;

	@GetMapping("/")
	public String index(Calculation calculation) {
		return "redirect:/calculator";
	}

	@GetMapping("/calculator")
	public String root(Calculation calculation) {
		return CALCULATOR_TEMPLATE;
	}

	@PostMapping("/calculator")
	public String calculate(Calculation calculation, BindingResult bindingResult, Model model) {
		final CalculationType type = CalculationType.valueOf(calculation.getCalculationType());
		final CalculationModel calculationModel = new CalculationModel(calculation.getLeftArgument(),
				calculation.getRightArgument(), type);

		final CalculationModel response = calculationService.calculate(calculationModel);

		model.addAttribute("response", response);
		return CALCULATOR_TEMPLATE;
	}

}

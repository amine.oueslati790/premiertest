package com.moi.testing.calcul.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.moi.testing.calcul.service.GreetingService;

@Controller
public class HomeController {

	private final GreetingService service;

	public HomeController(GreetingService service) {
		this.service = service;
	}

	@RequestMapping("/home")
	public @ResponseBody String greeting() {
		return service.greet();
	}
}
